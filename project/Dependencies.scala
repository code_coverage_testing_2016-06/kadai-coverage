import sbt._
import Keys._

object Dependencies {

  object Version {
    val scala211       = "2.11.7"
    val scala210       = "2.10.5"
    val scalaMacros    = "2.0.1"
    val scalaz         = "7.1.2"
    val scalazStream   = "0.7a"
    val specs2         = "3.6"
    val scalacheck     = "1.12.2"
    val junit          = "4.11"
    val scodecBits     = "1.0.6"
    val typesafeConfig = "1.3.0"
    val shapeless      = "2.0.0"
    val nscalaTime     = "2.0.0"
  }

  lazy val commonTest = 
    Seq(
      "org.specs2"          %% "specs2-core"        % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-junit"       % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-scalacheck"  % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-mock"        % Version.specs2     % "test"
    , "org.scalacheck"      %% "scalacheck"         % Version.scalacheck % "test"
    , "junit"                % "junit"              % Version.junit      % "test"
    )

  lazy val scalaz =
    Seq(
      "org.scalaz"        %% "scalaz-core"               % Version.scalaz
    , "org.scalaz"        %% "scalaz-concurrent"         % Version.scalaz
    , "org.scalaz"        %% "scalaz-effect"             % Version.scalaz
    , "org.scalaz"        %% "scalaz-scalacheck-binding" % Version.scalaz  % "test"
    )

  lazy val scalazStream = 
    Seq("org.scalaz.stream" %% "scalaz-stream"      % Version.scalazStream)

  lazy val typesafeConfig = 
    Seq("com.typesafe"              % "config"      % Version.typesafeConfig)

  lazy val scodecBits = 
    Seq("org.scodec"        %% "scodec-bits"        % Version.scodecBits)

  lazy val shapeless = 
    Seq(
      "com.chuusai"   %% "shapeless"   % Version.shapeless cross CrossVersion.binaryMapped {
        case "2.10" => "2.10.3"
        case x => x
      }
    )

  lazy val nscalaTime = 
    Seq("com.github.nscala-time"   %% "nscala-time" % Version.nscalaTime)
}
