/* 
 * Copyright 2013 Atlassian PTY LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kadai

import Throwables.ShowThrowable
import scalaz.{ Cord, Equal, Monoid, NonEmptyList, Show, \/ }
import scalaz.std.string._
import scalaz.syntax.either._
import scalaz.syntax.monoid._
import scalaz.syntax.nel._
import scalaz.syntax.show._
import scalaz.syntax.Ops

/**
 * ADT representing something invalid, may be a simple message or an exception.
 *
 * Contains syntax for converting a String or Throwable into an Invalid, eg:
 *
 * {{{
 * import Invalid.syntax
 *
 * val is: Invalid = "something went wrong".invalid
 * val or: Invalid \/ Int = "oh noes!".invalidResult[Int]
 * val nit: NonEmptyList[Invalid] = new IllegalStateException.invalidNel
 * }}}
 */
sealed trait Invalid {
  def toThrowable: Throwable =
    Invalid.WrappedException(this)
}

object Invalid {

  case class Message(s: String) extends Invalid
  case class Err(x: Throwable) extends Invalid {
    // need to implement custom equality as Throwable does not define equals
    override def equals(that: Any) = that match {
      case Err(t) if (t.getClass == x.getClass) => (t.getMessage == x.getMessage)
      case _                                    => false
    }
    override def hashCode = x.getClass.hashCode + Option(x.getMessage).hashCode
  }
  case class Composite(is: NonEmptyList[Invalid]) extends Invalid
  object Zero extends Invalid

  class ConvertTo(val invalid: Invalid) {
    final def invalidResult[A]: Invalid \/ A = invalid.left
    final def invalidNel: NonEmptyList[Invalid] = invalid.wrapNel
  }

  object syntax extends ToInvalidSyntax

  implicit object ShowInvalid extends Show[Invalid] {
    override def show(inv: Invalid): Cord =
      inv match {
        case Message(m)   => m
        case Err(e)       => e.show
        case Composite(l) => l.show
        case Zero         => "unknown".show
      }
  }

  case class WrappedException(invalid: Invalid) extends RuntimeException(implicitly[Show[Invalid]].shows(invalid)) {
    invalid match {
      case Invalid.Err(t) => initCause(t)
      case _              =>
    }
  }

  implicit object EqualInvalid extends Equal[Invalid] {
    def equal(a: Invalid, b: Invalid) = a == b
  }

  implicit object InvalidMonoid extends Monoid[Invalid] {
    def zero = Zero
    def append(a1: Invalid, a2: => Invalid) =
      (a1, a2) match {
        case (l, Zero)                      => l
        case (Zero, r)                      => r
        case (Composite(as), Composite(bs)) => Composite(as |+| bs)
        case (Composite(ls), r)             => Composite(ls |+| NonEmptyList(r))
        case (l, Composite(rs))             => Composite(NonEmptyList(l) |+| rs)
        case (l, r)                         => Composite(NonEmptyList(l, r))
      }
  }

  trait ToInvalidSyntax {
    implicit class AddInvalidToString(s: String) extends Invalid.ConvertTo(Invalid.Message(s))

    implicit class AddInvalidToThrowable(x: Throwable) extends Invalid.ConvertTo(Invalid.Err(x))
  }
}
