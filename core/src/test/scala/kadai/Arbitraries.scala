package kadai

import Invalid.syntax._
import org.scalacheck.{ Arbitrary, Gen, Prop }

object Arbitraries {
  implicit def ArbitraryInvalid: Arbitrary[Invalid] =
    Arbitrary {
      Gen.oneOf(
        Arbitrary.arbitrary[Throwable].map { _.invalid },
        Arbitrary.arbitrary[String].map { _.invalid }
      )
    }

  implicit def arbitraryAttempt[A: Arbitrary]: Arbitrary[Attempt[A]] =
    Arbitrary {
      import Attempt._

      Gen.frequency(
        1 -> Arbitrary.arbitrary[Throwable].map { exception },
        1 -> Arbitrary.arbitrary[String].map { fail },
        9 -> Arbitrary.arbitrary[A].map { ok }
      )
    }
}
