package kadai

import Invalid._
import Invalid.syntax._
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.scalacheck.Prop
import org.specs2.{ ScalaCheck, Specification }
import scalaz.scalacheck.ScalazProperties._
import scalaz.{ Monoid, NonEmptyList, -\/ }
import scalaz.syntax.std.boolean._

@RunWith(classOf[JUnitRunner])
class InvalidSpec extends ScalaCheckSpec {
  import Arbitraries._

  def is = s2"""
  Result should
    invalid creatable from a String               $invalidFromString
    invalid creatable from a Throwable            $invalidFromThrowable
    invalid has a law abiding Monoid              ${checkAll(monoid.laws[Invalid])}
  """

  def invalidFromString =
    Prop.forAll { s: String =>
      { s.invalid mustEqual Message(s) } and
        { s.invalidNel mustEqual NonEmptyList(Message(s)) } and
        { s.invalidResult mustEqual -\/(Message(s)) } and
        { s.invalid.toThrowable.getMessage mustEqual s }
    }

  def invalidFromThrowable =
    Prop.forAll { t: Throwable =>
      { t.invalid mustEqual Err(t) } and
        { t.invalidNel mustEqual NonEmptyList(Err(t)) } and
        { t.invalidResult mustEqual -\/(Err(t)) } and
        { t.invalid.toThrowable.getCause mustEqual t }
    }
}
