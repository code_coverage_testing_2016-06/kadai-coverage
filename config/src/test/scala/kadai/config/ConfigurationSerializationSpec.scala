package kadai.config

class ConfigurationSerializationSpec extends org.specs2.Specification {
  def is = s2"""A Configuration should be serializable $serialize"""

  def serialize = {
    import java.io._
    val byteStream = new ByteArrayOutputStream()
    val out = new ObjectOutputStream(byteStream)
    val conf = Configuration from "config {}"
    out.writeObject(conf)
    byteStream.toByteArray().size must be >= 1
  }
}