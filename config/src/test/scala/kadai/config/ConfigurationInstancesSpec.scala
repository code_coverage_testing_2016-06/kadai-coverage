package kadai
package config

import java.util.concurrent.TimeUnit

import concurrent.duration.Duration

import org.scalacheck.{ Gen, Prop }
import org.scalacheck.Arbitrary.{ arbLong, arbString }
import org.specs2.{ ScalaCheck, Specification }
import org.specs2.specification.core.Fragments

import com.typesafe.config.ConfigException

class ConfigurationInstancesSpec extends Specification with ScalaCheck {

  def is = s2"""
    TimeUnit accessor
      pass known time units   $timeUnitPass
      fail unknown time units $timeUnitFail

    Duration accessor
      pass known durations    $durationPass
      fail unknown durations  $durationFail
  """

  def timeUnitPass =
    Fragments.foreach(allTimeUnitNames) {
      case (name, unit) => checkTimeUnit(name, unit)
    }

  private def checkTimeUnit(name: String, expected: TimeUnit) =
    br ^ s"      $name" ! {
      Configuration.from(s"""foo = $name""").get[TimeUnit]("foo") === expected
    }

  def timeUnitFail = Prop.forAll { (s: String) =>
    !allTimeUnitNames.exists { case (n, _) => n == s } ==> {
      Configuration.from(s"foo = $s ").get[TimeUnit]("foo") must throwAn[ConfigException]
    }
  }

  val all =
    List(
      "ms" -> TimeUnit.MILLISECONDS,
      "milli" -> TimeUnit.MILLISECONDS,
      "millis" -> TimeUnit.MILLISECONDS,
      "millisecond" -> TimeUnit.MILLISECONDS,
      "milliseconds" -> TimeUnit.MILLISECONDS,
      "us" -> TimeUnit.MICROSECONDS,
      "micro" -> TimeUnit.MICROSECONDS,
      "micros" -> TimeUnit.MICROSECONDS,
      "microsecond" -> TimeUnit.MICROSECONDS,
      "microseconds" -> TimeUnit.MICROSECONDS,
      "ns" -> TimeUnit.NANOSECONDS,
      "nano" -> TimeUnit.NANOSECONDS,
      "nanos" -> TimeUnit.NANOSECONDS,
      "nanosecond" -> TimeUnit.NANOSECONDS,
      "nanoseconds" -> TimeUnit.NANOSECONDS,
      "d" -> TimeUnit.DAYS,
      "day" -> TimeUnit.DAYS,
      "days" -> TimeUnit.DAYS,
      "h" -> TimeUnit.HOURS,
      "hr" -> TimeUnit.HOURS,
      "hour" -> TimeUnit.HOURS,
      "hours" -> TimeUnit.HOURS,
      "s" -> TimeUnit.SECONDS,
      "sec" -> TimeUnit.SECONDS,
      "secs" -> TimeUnit.SECONDS,
      "second" -> TimeUnit.SECONDS,
      "seconds" -> TimeUnit.SECONDS,
      "m" -> TimeUnit.MINUTES,
      "min" -> TimeUnit.MINUTES,
      "mins" -> TimeUnit.MINUTES,
      "minute" -> TimeUnit.MINUTES,
      "minutes" -> TimeUnit.MINUTES
    )

  def durationPass = Prop.forAll(Gen.oneOf(typesafeConfigDefaultDurationUnits)) {
    case ((name: String, unit: TimeUnit)) =>
      // typesafe config parse negative duration using Double instead of Long, so the precision is lost
      Prop.forAll(Gen.chooseNum[Long](0, MaxDurations(unit))) { (i: Long) =>
        Configuration.from(s"""foo = $i $name""").get[Duration]("foo") === Duration(i, unit)
      }
  }

  def durationFail = Prop.forAll { (i: Long, s: String) =>
    !typesafeConfigDefaultDurationUnits.exists { case (n, _) => n == s } ==> {
      Configuration.from(s"foo = $i $s").get[Duration]("foo") must throwAn[ConfigException]
    }
  }

  val typesafeConfigDefaultDurationUnits = List(
    "" -> TimeUnit.MILLISECONDS,
    "ms" -> TimeUnit.MILLISECONDS,
    "millisecond" -> TimeUnit.MILLISECONDS,
    "milliseconds" -> TimeUnit.MILLISECONDS,
    "us" -> TimeUnit.MICROSECONDS,
    "microsecond" -> TimeUnit.MICROSECONDS,
    "microseconds" -> TimeUnit.MICROSECONDS,
    "ns" -> TimeUnit.NANOSECONDS,
    "nanosecond" -> TimeUnit.NANOSECONDS,
    "nanoseconds" -> TimeUnit.NANOSECONDS,
    "d" -> TimeUnit.DAYS,
    "day" -> TimeUnit.DAYS,
    "days" -> TimeUnit.DAYS,
    "h" -> TimeUnit.HOURS,
    "hour" -> TimeUnit.HOURS,
    "hours" -> TimeUnit.HOURS,
    "s" -> TimeUnit.SECONDS,
    "second" -> TimeUnit.SECONDS,
    "seconds" -> TimeUnit.SECONDS,
    "m" -> TimeUnit.MINUTES,
    "minute" -> TimeUnit.MINUTES,
    "minutes" -> TimeUnit.MINUTES
  )

  val allTimeUnitNames =
    List(
      "milli" -> TimeUnit.MILLISECONDS,
      "millis" -> TimeUnit.MILLISECONDS,
      "micro" -> TimeUnit.MICROSECONDS,
      "micros" -> TimeUnit.MICROSECONDS,
      "nano" -> TimeUnit.NANOSECONDS,
      "nanos" -> TimeUnit.NANOSECONDS,
      "hr" -> TimeUnit.HOURS,
      "sec" -> TimeUnit.SECONDS,
      "secs" -> TimeUnit.SECONDS,
      "min" -> TimeUnit.MINUTES,
      "mins" -> TimeUnit.MINUTES
    ) ::: typesafeConfigDefaultDurationUnits.filterNot { case (u, _) => u == "" }

  // Calculate the maximum duration for each time unit, which equals to the max defined in Scala's FiniteDuration.
  private val MaxDurations =
    List(
      TimeUnit.NANOSECONDS,
      TimeUnit.MICROSECONDS,
      TimeUnit.MILLISECONDS,
      TimeUnit.SECONDS,
      TimeUnit.MINUTES,
      TimeUnit.HOURS,
      TimeUnit.DAYS
    ).map { unit => unit -> Long.MaxValue / Duration(1, unit).toNanos }.toMap
}
