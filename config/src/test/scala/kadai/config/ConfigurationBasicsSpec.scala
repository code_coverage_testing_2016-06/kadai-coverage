package kadai.config

import org.scalacheck.Prop
import org.specs2.{ ScalaCheck, Specification }

import scalaz.syntax.id._

class ConfigurationBasicsSpec extends Specification with ScalaCheck {
  def is = s2"""
  Configuration must
  
    be equal                            $equalTest
    be not equal with differing keys    $notEqualKeys
    be not equal with differing values  $notEqualValues
  """

  def equalTest = 
    (Configuration.from("test = test") -> Configuration.from("test = test")) |> {
      case (c1, c2) => (c1 === c2) and (c1.## === c2.##)
    }

  def notEqualKeys = 
    (Configuration.from(s"test = test") -> Configuration.from("best = test")) |> {
      case (c1, c2) => (c1 !== c2) and (c1.## !== c2.##)
    }

  def notEqualValues = 
    (Configuration.from(s"test = test") -> Configuration.from("test = rest")) |> {
      case (c1, c2) => (c1 !== c2) and (c1.## !== c2.##)
    }
}