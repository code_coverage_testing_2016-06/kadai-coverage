# Coverage Testing

THIS IS NOT THE OFFICIAL KADAI REPOSITORY.

This is a copy being used to test various Scala code coverage tools.

[![Coverage Status](https://coveralls.io/repos/bitbucket/code_coverage_testing_2016-06/kadai-coverage/badge.svg?branch=master)](https://coveralls.io/bitbucket/code_coverage_testing_2016-06/kadai-coverage?branch=master)

# kadai

## General Scala utilities

kadai is a collection of general utility classes that are helpful when starting
a new Scala project. These have been derived from a number of projects that were
repeating essentially the same code.

## Architecture

The code is separated into fairly specific purpose modules, each with as few dependencies as possible.

The modules are:

### core

basic data structures for disjunction result types with Invalids holding errors or error messages
on the left and and utilities for turning exceptions into Invalid results

### result

`ResultT[F, A]` is a monad transformer that specializes `EitherT[F, Invalid, A]` ie. it partially applies
`Invalid` as the error type. `Result[A]` specializes `ResultT[Id, A]`

### config

simple Scala interface to the typesafe-config library with a Reader monad for composing 
configurable things and injecting configuration files. `ConfigResult` puts the two together.

### hash

Tools for hashing bytes

### cmdopts

simple and type-safe Command Line Option parser

### concurrent

commonly useful concurrency primitives

# Download

To use this library from SBT, add the following library dependency:

    libraryDependencies += "io.atlassian" %% "kadai" % "3.3.1"

There is an artifactId: `kadai` that combines all modules or you can depend on one directly with `kadai-core`, `kadai-cmdopts` etc. Note:, there is a scala-version qualifier.
